from random import randint
import os

def building_battlefield(v):
	v = {}
	c = -1
	l = 1
	a = [str(a) for a in range(1,11)]
	b = [chr(b) for b in (range(ord('a'), ord('k')))]
	z = []
	for i in range(1, 11):
		c += 1
		if c == 10:
			c = 0
		for m in range(0, 10):
			z.append(a[c] + b[m])
	for i in range(len(z)):
		if i % 11 == 0:
			z.insert(i, str(l))
			l +=1
	for i in z:
		if 'j' in i:
			v[i] = ' ~\n'
		elif len(i) == 1:
			v[i] = i + '  '
		elif i == '10':
			v[i] = '10 '
		else:	
			v[i] = ' ~'
	return v

hidden_battlefield = {}
opponent_battlefield = {}
player_battlefield = {}

hidden_battlefield =  building_battlefield(hidden_battlefield)
opponent_battlefield = building_battlefield(opponent_battlefield)
player_battlefield = building_battlefield(player_battlefield)

z = []
for  i in range(1,12):
	z.append(str(i) + chr(96))

for i in range(ord('a'), ord('l')):
	z.append('11' + chr(i))

for i in range(0,11):
	z.append(str(i) + 'k')
for i in range(ord('`'), ord('k')):
	z.append('0' + chr(i))

for i in z:
	player_battlefield[i] = ''

def border_crossing_prevention(integer, letter, operation):
	plusi = 0
	minusi = 0
	plusl = 0
	minusl = 0
	if operation == 'plusl':
		plusl = letter          
		if plusl + 1 != 107:
			plusl = plusl + 1
		p = plusl
	elif operation == 'plusi':	
		plusi = integer
		if plusi + 1 != 11:
			plusi = plusi + 1
		p = plusi			
	elif operation == 'minusi':	
		minusi = integer
		if minusi - 1 != 0:
			minusi = minusi - 1
		p = minusi			
	elif operation == 'minusl':	
		minusl = letter
		if minusl - 1 != 96:
			minusl = minusl - 1
		p = minusl			
	return p

def building_area_around_ships(gorvert, integer, letter, istorpednik, shiptype, amountships): #
	vokrugkorablya = []
	x = []
	
	if gorvert == 0:
		x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))
		x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
		x.append((str(integer) + chr(border_crossing_prevention(integer, letter, 'minusl'))))
		x.append((str(integer) + chr(border_crossing_prevention(integer, letter, 'plusl'))))
		x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
		x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))		
	elif gorvert == 1:
		x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))
		x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
		x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(letter))
		x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(letter))
		x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
		x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))
	if istorpednik == 1:
		if gorvert == 0: 
			x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))
			x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
			x.append((str(integer) + chr(border_crossing_prevention(integer, letter, 'minusl'))))
			x.append((str(integer) + chr(border_crossing_prevention(integer, letter, 'plusl'))))
			x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(letter))
			x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(letter))
			x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
			x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
			x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))
		elif gorvert == 1:
			x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))
			x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
			x.append((str(integer) + chr(border_crossing_prevention(integer, letter, 'minusl'))))
			x.append((str(integer) + chr(border_crossing_prevention(integer, letter, 'plusl'))))
			x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(letter))
			x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(letter))
			x.append((str(border_crossing_prevention(integer, letter, 'plusi'))) + chr(border_crossing_prevention(integer, letter, 'minusl')))
			x.append((str(border_crossing_prevention(integer, letter, 'minusi'))) + chr(border_crossing_prevention(integer, letter, 'plusl')))
	
	if istorpednik == 1:	
		if shiptype == 'torpednik' and amountships == '0':
			vokrugtorpednik0 = []
			vokrugtorpednik0 = x
			return vokrugtorpednik0
		elif shiptype == 'torpednik' and amountships == '1':
			vokrugtorpednik1 = []
			vokrugtorpednik1 = x
			return vokrugtorpednik1
		if shiptype == 'torpednik' and amountships == '2':
			vokrugtorpednik2 = []
			vokrugtorpednik2 = x
			return vokrugtorpednik2
		if shiptype == 'torpednik' and amountships == '3':
			vokrugtorpednik3 = []
			vokrugtorpednik3 = x
			return vokrugtorpednik3
	elif istorpednik == 0:
		if shiptype == 'linkor':
			vokruglinkor0 = []
			vokruglinkor0 = x
			return vokruglinkor0
		if shiptype == 'kreiser' and amountships == '0':
			vokrugkreiser0 = []
			vokrugkreiser0 = x
			return vokrugkreiser0
		elif shiptype == 'kreiser' and amountships == '1':
			vokrugkreiser1 = []
			vokrugkreiser1 = x
			return vokrugkreiser1
		if shiptype == 'aesminec' and amountships == '0':
			vokrugaesminec0 = []
			vokrugaesminec0 = x
			return vokrugaesminec0
		elif shiptype == 'aesminec' and amountships == '1':
			vokrugaesminec1 = []
			vokrugaesminec1 = x
			return vokrugaesminec1
		elif shiptype == 'aesminec' and amountships == '2':
			vokrugaesminec2 = []
			vokrugaesminec2 = x
			return vokrugaesminec2			
korabli = []
def bulding_battleships():
	linkor = []
	vokruglinkora = []
	integer = randint(1, 10)
	letter = randint(97, 106)
	gorvert = randint(0,1)
	amountships = 0
	while amountships != 1:
		if gorvert == 0:
			if letter > 102:
				for i in range(4):
					linkor.append(str(integer) + chr(letter))
					vokruglinkora += building_area_around_ships(0, integer, letter, 0, 'linkor', str(amountships))
					letter -= 1
			else:
				for i in range(4):	
					linkor.append(str(integer) + chr(letter))
					vokruglinkora += building_area_around_ships(0, integer, letter, 0, 'linkor', str(amountships))
					letter += 1
		elif gorvert == 1:
			if integer > 6:
				for i in range(4):
					linkor.append(str(integer) + chr(letter))
					vokruglinkora += building_area_around_ships(1, integer, letter, 0, 'linkor', str(amountships))
					integer -= 1
			else:
				for i in range(4):
					linkor.append(str(integer) + chr(letter))
					vokruglinkora += building_area_around_ships(1, integer, letter, 0, 'linkor', str(amountships))
					integer += 1
		amountships += 1
	vokruglinkora = list(set(vokruglinkora) - set(linkor))

	kreiser0 = []
	kreiser1 = []
	kreisers = []
	vokrugkreiserov = []
	vokrugkreiser0 = []
	vokrugkreiser1 = []
	amountships = 0
	while amountships != 2:		
		while (str(integer) + chr(letter)) in (vokruglinkora + linkor + kreiser0 + kreiser1 + vokrugkreiser0 + vokrugkreiser1):  
			integer = randint(1, 10)
			letter = randint(97, 106)
			gorvert = randint(0,1)
		if gorvert == 0:
			if letter > 103:
				for i in range(3):
					if amountships == 0:
						kreiser0.append(str(integer) + chr(letter))
					if amountships == 1:
						kreiser1.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugkreiser0 += building_area_around_ships(0, integer, letter, 0, 'kreiser', str(amountships))
					if amountships == 1:
						vokrugkreiser1 += building_area_around_ships(0, integer, letter, 0, 'kreiser', str(amountships))
					letter -= 1
			else:
				for i in range(3):
					if amountships == 0:
						kreiser0.append(str(integer) + chr(letter))
					if amountships == 1:
						kreiser1.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugkreiser0 += building_area_around_ships(0, integer, letter, 0, 'kreiser', str(amountships))
					if amountships == 1:
						vokrugkreiser1 += building_area_around_ships(0, integer, letter, 0, 'kreiser', str(amountships))
					letter += 1
		elif gorvert == 1:
			if integer > 7:
				for i in range(3):
					if amountships == 0:
						kreiser0.append(str(integer) + chr(letter))
					if amountships == 1:
						kreiser1.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugkreiser0 += building_area_around_ships(1, integer, letter, 0, 'kreiser', str(amountships))
					if amountships == 1:
						vokrugkreiser1 += building_area_around_ships(1, integer, letter, 0, 'kreiser', str(amountships))
					integer -= 1
			else:
				for i in range(3):
					if amountships == 0:
						kreiser0.append(str(integer) + chr(letter))
					if amountships == 1:
						kreiser1.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugkreiser0 += building_area_around_ships(1, integer, letter, 0, 'kreiser', str(amountships))
					if amountships == 1:
						vokrugkreiser1 += building_area_around_ships(1, integer, letter, 0, 'kreiser', str(amountships))
					integer += 1		
		vokrugkreiser0 = list(set(vokrugkreiser0) - set(kreiser0))
		vokrugkreiser1 = list(set(vokrugkreiser1) - set(kreiser1))
		if set(kreiser0 + kreiser1) & set(vokruglinkora + vokrugkreiser0 + vokrugkreiser1):
			kreiser0 = []
			kreiser1 = []
			vokrugkreiser0 = []
			vokrugkreiser1 = []
			amountships = -1
			continue
		amountships += 1
	vokrugkreiser0 = list(set(vokrugkreiser0) - set(kreiser0))
	vokrugkreiser1 = list(set(vokrugkreiser1) - set(kreiser1))
	kreisers = kreiser0 + kreiser1
	vokrugkreiserov = vokrugkreiser0 + vokrugkreiser1

	aesmincbi = []
	vokrugaesmincev = []
	aesminec0 = []
	aesminec1 = []
	aesminec2 = []
	vokrugaesminec0 = []
	vokrugaesminec1 = []
	vokrugaesminec2 = []
	amountships = 0
	while amountships != 3:
		while (str(integer) + chr(letter)) in (linkor + vokruglinkora + vokrugkreiserov + kreisers + aesminec0 + vokrugaesminec0 + aesminec1 + vokrugaesminec1 + aesminec2 + vokrugaesminec2): #  + aesmincbi + vokrugaesminca  
			integer = randint(1, 10)
			letter = randint(97, 106)
		gorvert = randint(0,1)
		if gorvert == 0:
			if letter > 104:
				for i in range(2):
					if amountships == 0:
						aesminec0.append(str(integer) + chr(letter))
					if amountships == 1:
						aesminec1.append(str(integer) + chr(letter))
					if amountships == 2:
						aesminec2.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugaesminec0 += building_area_around_ships(0, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 1:
						vokrugaesminec1 += building_area_around_ships(0, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 2:
						vokrugaesminec2 += building_area_around_ships(0, integer, letter, 0, 'aesminec', str(amountships))
					letter -= 1
			else:
				for i in range(2):
					if amountships == 0:
						aesminec0.append(str(integer) + chr(letter))
					if amountships == 1:
						aesminec1.append(str(integer) + chr(letter))
					if amountships == 2:
						aesminec2.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugaesminec0 += building_area_around_ships(0, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 1:
						vokrugaesminec1 += building_area_around_ships(0, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 2:
						vokrugaesminec2 += building_area_around_ships(0, integer, letter, 0, 'aesminec', str(amountships))
					letter += 1
		elif gorvert == 1:
			if integer > 8:
				for i in range(2):
					if amountships == 0:
						aesminec0.append(str(integer) + chr(letter))
					if amountships == 1:
						aesminec1.append(str(integer) + chr(letter))
					if amountships == 2:
						aesminec2.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugaesminec0 += building_area_around_ships(1, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 1:
						vokrugaesminec1 += building_area_around_ships(1, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 2:
						vokrugaesminec2 += building_area_around_ships(1, integer, letter, 0, 'aesminec', str(amountships))
					integer -= 1
			else:
				for i in range(2):
					if amountships == 0:
						aesminec0.append(str(integer) + chr(letter))
					if amountships == 1:
						aesminec1.append(str(integer) + chr(letter))
					if amountships == 2:
						aesminec2.append(str(integer) + chr(letter))
					if amountships == 0:
						vokrugaesminec0 += building_area_around_ships(1, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 1:
						vokrugaesminec1 += building_area_around_ships(1, integer, letter, 0, 'aesminec', str(amountships))
					if amountships == 2:
						vokrugaesminec2 += building_area_around_ships(1, integer, letter, 0, 'aesminec', str(amountships))
					integer += 1
		vokrugaesminec0 = list(set(vokrugaesminec0) - set(aesminec0))
		vokrugaesminec1 = list(set(vokrugaesminec1) - set(aesminec1))
		vokrugaesminec2 = list(set(vokrugaesminec2) - set(aesminec2))
		if set(aesminec0 + aesminec1 + aesminec2) & set(vokrugkreiserov + vokruglinkora + vokrugaesminec0 + vokrugaesminec1 + vokrugaesminec2):
			aesminec0 = []
			aesminec1 = []
			aesminec2 = []
			vokrugaesminec0 = []
			vokrugaesminec1 = []
			vokrugaesminec2 = []
			amountships = -1
			continue
		amountships += 1
	vokrugaesminec0 = list(set(vokrugaesminec0) - set(aesminec0))
	vokrugaesminec1 = list(set(vokrugaesminec1) - set(aesminec1))
	vokrugaesminec2 = list(set(vokrugaesminec2) - set(aesminec2))
	aesmincbi = aesminec0 + aesminec1 + aesminec2
	vokrugaesmincev = vokrugaesminec0 + vokrugaesminec1 + vokrugaesminec2
	
	torpednik0 = []
	torpednik1 = []
	torpednik2 = []
	torpednik3 = []
	vokrugtorpednik0 = []
	vokrugtorpednik1 = []
	vokrugtorpednik2 = []
	vokrugtorpednik3 = []
	torpedniki = []
	vokrugtorpednikov = []
	amountships = 0		
	while amountships != 4:
		while (str(integer) + chr(letter)) in (vokruglinkora + linkor + vokrugkreiserov + kreisers + vokrugaesmincev + aesmincbi + torpednik0 + vokrugtorpednik0 + torpednik1 + vokrugtorpednik1 + torpednik2 + vokrugtorpednik2 + torpednik3 + vokrugtorpednik3):  #+ vokrugaesminca  + vokrugkreisera  + vokrugtorpednika + torpedniki
			integer = randint(1, 10)
			letter = randint(97, 106)
		gorvert = randint(0,1)
		if gorvert == 0:
			for i in range(1):
				if amountships == 0:
					torpednik0.append(str(integer) + chr(letter))
				if amountships == 1:
					torpednik1.append(str(integer) + chr(letter))
				if amountships == 2:
					torpednik2.append(str(integer) + chr(letter))
				if amountships == 3:
					torpednik3.append(str(integer) + chr(letter))
				if amountships == 0:
					vokrugtorpednik0 += building_area_around_ships(0, integer, letter, 1, 'torpednik', str(amountships))
				if amountships == 1:
					vokrugtorpednik1 += building_area_around_ships(0, integer, letter, 1, 'torpednik', str(amountships))
				if amountships == 2:
					vokrugtorpednik2 += building_area_around_ships(0, integer, letter, 1, 'torpednik', str(amountships))
				if amountships == 3:
					vokrugtorpednik3 += building_area_around_ships(0, integer, letter, 1, 'torpednik', str(amountships))
		elif gorvert == 1:
			for i in range(1):
				if amountships == 0:
					torpednik0.append(str(integer) + chr(letter))
				if amountships == 1:
					torpednik1.append(str(integer) + chr(letter))
				if amountships == 2:
					torpednik2.append(str(integer) + chr(letter))
				if amountships == 3:
					torpednik3.append(str(integer) + chr(letter))
				if amountships == 0:
					vokrugtorpednik0 += building_area_around_ships(1, integer, letter, 1, 'torpednik', str(amountships))
				if amountships == 1:
					vokrugtorpednik1 += building_area_around_ships(1, integer, letter, 1, 'torpednik', str(amountships))
				if amountships == 2:
					vokrugtorpednik2 += building_area_around_ships(1, integer, letter, 1, 'torpednik', str(amountships))
				if amountships == 3:
					vokrugtorpednik3 += building_area_around_ships(1, integer, letter, 1, 'torpednik', str(amountships))
		amountships += 1
	
	vokrugtorpednik0 = list(set(vokrugtorpednik0) - set(torpednik0))
	vokrugtorpednik1 = list(set(vokrugtorpednik1) - set(torpednik1))
	vokrugtorpednik2 = list(set(vokrugtorpednik2) - set(torpednik2))
	vokrugtorpednik3 = list(set(vokrugtorpednik3) - set(torpednik3))
	torpedniki = torpednik0 + torpednik1 + torpednik2 + torpednik3
	vokrugtorpednikov = vokrugtorpednik0 + vokrugtorpednik1 + vokrugtorpednik2 + vokrugtorpednik3

	korabli = linkor + kreisers + aesmincbi + torpedniki
	vokrugkorablei = vokruglinkora + vokrugkreiserov + vokrugaesmincev + vokrugtorpednikov
	return korabli, vokrugkorablei, linkor, vokruglinkora, kreiser0, vokrugkreiser0, kreiser1, vokrugkreiser1, aesminec0, vokrugaesminec0, aesminec1, vokrugaesminec1, aesminec2, vokrugaesminec2, torpednik0, vokrugtorpednik0, torpednik1, vokrugtorpednik1, torpednik2, vokrugtorpednik2, torpednik3, vokrugtorpednik3

bulding_battleships = bulding_battleships()
korabli = bulding_battleships[0]
vokrugkorablei = bulding_battleships[1]
linkor = bulding_battleships[2]
vokruglinkora = bulding_battleships[3]
kreiser0 =  bulding_battleships[4]
vokrugkreiser0 =  bulding_battleships[5]
kreiser1 =  bulding_battleships[6]
vokrugkreiser1 =  bulding_battleships[7]
aesminec0 =  bulding_battleships[8]
vokrugaesminec0 =  bulding_battleships[9]
aesminec1 =  bulding_battleships[10]
vokrugaesminec1 =  bulding_battleships[11]
aesminec2 =  bulding_battleships[12]
vokrugaesminec2 =  bulding_battleships[13]
torpednik0 =  bulding_battleships[14]
vokrugtorpednik0 =  bulding_battleships[15]
torpednik1 =  bulding_battleships[16]
vokrugtorpednik1 =  bulding_battleships[17]
torpednik2 =  bulding_battleships[18]
vokrugtorpednik2 =  bulding_battleships[19]
torpednik3 =  bulding_battleships[20]
vokrugtorpednik3 =  bulding_battleships[21]

for i in korabli:
	if '\n' in opponent_battlefield[i]:	
		opponent_battlefield[i] = ' #\n'	
	else:	
		opponent_battlefield[i] = ' #'

# print('    a b c d e f g h i j\n')
# print(''.join(opponent_battlefield.values()))

def building_playerbattlefield(z, t):	
	if t == 'linkor':
		player_battleships = battleship_linkor
	elif t == 'kreisers':
		player_battleships = battleship_linkor + ',' + battleships_kreisers
		playerkreiser0 = battleships_kreisers
	elif t == 'aesmincbi':
		player_battleships = battleship_linkor + ',' + battleships_kreisers + ',' + battleships_aesmincbi
	elif t == 'torpedniki':
		player_battleships = battleship_linkor + ',' + battleships_kreisers + ',' + battleships_aesmincbi + ',' + battleships_torpedniki
	return player_battleships

def placing_playerbattleships(s):
	player_battleships = s.split(',')
	for i in range(len(player_battleships)):
		if player_battlefield[player_battleships[i]] == ' ~\n':
			player_battlefield[player_battleships[i]] = ' #\n'
		elif player_battlefield[player_battleships[i]] == ' #\n':     # по какой-то непонятной причине лишает абсолютно не понятного бага
			player_battlefield[player_battleships[i]] = ' #\n'        # чтобы увидеть этот баг нужно без этой строки ввести корабли 10a,10b,10c,10d 10f,10g,10h 10j,9j,8j 8a,8b
		else:                          
			player_battlefield[player_battleships[i]] = ' #'
	return '    a b c d e f g h i j\n\n' + ''.join(player_battlefield.values())

def validating_input(inp, leninp, lenship):
	inporiginal = ''
	if '10' in inp:
		inporiginal = inp
		inp = inp.replace('0', '')
	if inp == 'stop':
		inp = 'stop'
	elif len(inp) != leninp:
		inp = 'again'
	elif inp.count(',') != lenship - 1:
		inp = 'again'
	elif len(set(list(inp.split(',')))) != len(list(inp.split(','))):
		inp = 'again'
	if '10' in inporiginal:
		inp = inp.replace('1', '10')
	return inp


countkreisers = 0
countaesmincbi = 0
counttorpedniki = 0
types_placed_ships = 0
while types_placed_ships != 4:
	os.system('cls')
	os.system('clear')
	print('\n       Твое поле боя\n')
	print('    a b c d e f g h i j\n')
	print(''.join(hidden_battlefield.values()))

	print('Размести линкор(длинна 4 ячейки)\nВведи обозначения ячеек(цифра и буква) через запятую, без пробелов\n')
	battleship_linkor = validating_input(input(), 11, 4)
	playerlinkor = battleship_linkor
	if 'again' in  battleship_linkor:
		continue
	elif 'stop' in battleship_linkor:
		print('\n  See u soon')
		exit()
	os.system('cls')
	os.system('clear')
	print('\n       Твое поле боя\n')
	print(placing_playerbattleships(building_playerbattlefield(battleship_linkor, 'linkor')))
	types_placed_ships +=1
	while countkreisers != 2:
		print('По одному размести 2 крейсера(длинна 3 ячейки)\n')
		if countkreisers == 0:
			playerkreiser0 = validating_input(input(), 8, 3)
			battleships_kreisers = playerkreiser0
		elif countkreisers == 1:
			playerkreiser1 = validating_input(input(), 8, 3)
			battleships_kreisers = playerkreiser0 + ',' + playerkreiser1
		if 'again' in battleships_kreisers:
			continue
		if 'stop' in battleships_kreisers:
			print('\n  See u soon')
			exit()
		os.system('cls')
		os.system('clear')
		print('\n       Твое поле боя\n')
		print(placing_playerbattleships(building_playerbattlefield(battleships_kreisers, 'kreisers')))
		countkreisers += 1
	types_placed_ships +=1
	while countaesmincbi != 3:
		print('По одному размести 3 эсминца(длинна 2 ячейки)\n')
		if countaesmincbi == 0:
			playeraesminec0 = validating_input(input(), 5, 2)
			battleships_aesmincbi = playeraesminec0
		elif countaesmincbi == 1:	
			playeraesminec1 = validating_input(input(), 5, 2)
			battleships_aesmincbi = playeraesminec0 + ',' + playeraesminec1
		elif countaesmincbi == 2:
			playeraesminec2 = validating_input(input(), 5, 2)
			battleships_aesmincbi = playeraesminec0 + ',' + playeraesminec1 + ',' + playeraesminec2
		if 'again' in battleships_aesmincbi:
			continue
		if 'stop' in battleships_aesmincbi:
			print('\n  See u soon')
			exit()
		os.system('cls')
		os.system('clear')
		print('\n       Твое поле боя\n')
		print(placing_playerbattleships(building_playerbattlefield(battleships_aesmincbi, 'aesmincbi')))
		countaesmincbi += 1
	types_placed_ships += 1
	while counttorpedniki != 4:
		print('По одному размести 4 торпедных катера(длинна 1 ячейка)\n')
		if counttorpedniki == 0:
			playertorpednik0 = validating_input(input(), 2, 1)
			battleships_torpedniki = playertorpednik0
		if counttorpedniki == 1:
			playertorpednik1 = validating_input(input(), 2, 1)
			battleships_torpedniki = playertorpednik0 + ',' + playertorpednik1
		if counttorpedniki == 2:
			playertorpednik2 = validating_input(input(), 2, 1)
			battleships_torpedniki = playertorpednik0 + ',' + playertorpednik1 + ',' + playertorpednik2
		if counttorpedniki == 3:
			playertorpednik3 = validating_input(input(), 2, 1)
			battleships_torpedniki = playertorpednik0 + ',' + playertorpednik1 + ',' + playertorpednik2 + ',' + playertorpednik3
		if 'again' in battleships_torpedniki:
			continue
		if 'stop' in battleships_torpedniki:
			print('\n  See u soon')
			exit()
		os.system('cls')
		os.system('clear')
		print('\n       Твое поле боя\n')
		print(placing_playerbattleships(building_playerbattlefield(battleships_torpedniki, 'torpedniki')))
		counttorpedniki += 1
	types_placed_ships += 1

def border_crossing_prevention_for_playerships(somevalue):
	if '`' in somevalue:
		somevalue = 'не, хуйня'
	if 'k' in somevalue:
		somevalue = 'не, хуйня'
	if '11' in somevalue:
		somevalue = 'не, хуйня'
	if '9' not in somevalue and '10' not in somevalue:	
		if '0' in somevalue:
			somevalue = 'не, хуйня'
	if somevalue == 'не, хуйня':
		return False
	else:
		return True

def building_area_around_player_ships(playership):

# playerlinkor = '5e,5f,5g,5h'
#playerlinkor = '4e,5e,6e,7e'
# playership = input()

	if len(playership) == 2:
		playership = playership + ',' + playership
	if '10' not in playership:	
		if playership.split(',')[0][0] in playership.split(',')[1]:
			playership = playership.split(',')
			aroundplayership = playership.copy()
			if border_crossing_prevention_for_playerships(playership[0][0] + chr(ord(playership[0][1]) - 1)) == True:
				aroundplayership.insert(0, playership[0][0] + chr(ord(playership[0][1]) - 1))
			if border_crossing_prevention_for_playerships(playership[0][0] + chr(ord(playership[-1][1]) + 1)) == True:
				aroundplayership.append(playership[0][0] + chr(ord(playership[-1][1]) + 1))
			aroundplayership1 = aroundplayership.copy()
			for i in aroundplayership:
				if border_crossing_prevention_for_playerships(i.replace(playership[0][0], str(int(playership[0][0]) + 1))) == True:
					aroundplayership1.append(i.replace(playership[0][0], str(int(playership[0][0]) + 1)))
				if border_crossing_prevention_for_playerships(i.replace(playership[0][0], str(int(playership[0][0]) - 1))) == True:
					aroundplayership1.append(i.replace(playership[0][0], str(int(playership[0][0]) - 1)))
			aroundplayership1 = set(aroundplayership1) - set(playership)
		else:
			playership = playership.split(',')
			aroundplayership = playership.copy()
			if border_crossing_prevention_for_playerships((str(int(playership[0][0]) - 1)) + playership[0][1]) == True:
				aroundplayership.insert(0, (str(int(playership[0][0]) - 1)) + playership[0][1])
			if border_crossing_prevention_for_playerships(str((int(playership[-1][0]) + 1)) + playership[-1][1]) == True:
				aroundplayership.append(str((int(playership[-1][0]) + 1)) + playership[-1][1])
			aroundplayership1 = aroundplayership.copy()
			for i in aroundplayership:
				print(playership[0][1])
				if border_crossing_prevention_for_playerships(i.replace(playership[0][1], chr(ord(playership[0][1]) + 1))) == True:
					aroundplayership1.append(i.replace(playership[0][1], chr(ord(playership[0][1]) + 1)))
				if border_crossing_prevention_for_playerships(i.replace(playership[0][1], chr(ord(playership[0][1]) - 1))) == True:
					aroundplayership1.append(i.replace(playership[0][1], chr(ord(playership[0][1]) - 1)))
			aroundplayership1 = set(aroundplayership1) - set(playership)
	if '10' in playership:
		if len(playership) == 3:
			playership = playership + ',' + playership
		if playership.split(',')[0][0] in playership.split(',')[1]:
			playership = playership.split(',')
			aroundplayership = playership.copy()
			if border_crossing_prevention_for_playerships((playership[0][0] + playership[0][1]) + chr(ord(playership[0][2]) - 1)) == True:
				aroundplayership.insert(0, (playership[0][0] + playership[0][1]) + chr(ord(playership[0][2]) - 1))	
			if border_crossing_prevention_for_playerships((playership[0][0] + playership[0][1]) + chr(ord(playership[-1][2]) + 1)) == True:
				aroundplayership.append((playership[0][0] + playership[0][1]) + chr(ord(playership[-1][2]) + 1))
			aroundplayership1 = aroundplayership.copy()
			for i in aroundplayership:
				if border_crossing_prevention_for_playerships(i.replace((playership[0][0] + playership[0][1]), str(int((playership[0][0] + playership[0][1])) + 1))) == True:
					aroundplayership1.append(i.replace((playership[0][0] + playership[0][1]), str(int((playership[0][0] + playership[0][1])) + 1)))
				if border_crossing_prevention_for_playerships(i.replace((playership[0][0] + playership[0][1]), str(int((playership[0][0] + playership[0][1])) - 1))) == True:
					aroundplayership1.append(i.replace((playership[0][0] + playership[0][1]), str(int((playership[0][0] + playership[0][1])) - 1)))
			aroundplayership1 = set(aroundplayership1) - set(playership)
		else:
			playership = playership.split(',')
			aroundplayership = playership.copy()
			if border_crossing_prevention_for_playerships((str(int(playership[0][0] + playership[0][1]) - 1)) + playership[0][2]) == True:
				aroundplayership.insert(0, (str(int((playership[0][0] + playership[0][1])) - 1)) + playership[0][2])	
			if border_crossing_prevention_for_playerships(str((int(playership[-1][0] + playership[-1][1]) + 1)) + playership[-1][2]) == True:
				aroundplayership.append(str((int(playership[-1][0] + playership[-1][1]) + 1)) + playership[-1][2])
			aroundplayership1 = aroundplayership.copy()
			for i in aroundplayership:
				print(playership[0][1])
				if border_crossing_prevention_for_playerships(i.replace(playership[0][2], chr(ord(playership[0][2]) + 1))) == True:
					aroundplayership1.append(i.replace(playership[0][2], chr(ord(playership[0][2]) + 1)))
				if border_crossing_prevention_for_playerships(i.replace(playership[0][2], chr(ord(playership[0][2]) - 1))) == True:	
					aroundplayership1.append(i.replace(playership[0][2], chr(ord(playership[0][2]) - 1)))
			aroundplayership1 = set(aroundplayership1) - set(playership)
	return aroundplayership1

vokrugplayerlinkor = building_area_around_player_ships(playerlinkor)
vokrugplayerkreiser0 = building_area_around_player_ships(playerkreiser0)
vokrugplayerkreiser1 = building_area_around_player_ships(playerkreiser1)
vokrugplayeraesminec0 = building_area_around_player_ships(playeraesminec0)
vokrugplayeraesminec1 = building_area_around_player_ships(playeraesminec1)
vokrugplayeraesminec2 = building_area_around_player_ships(playeraesminec2)
vokrugplayertorpednik0 = building_area_around_player_ships(playertorpednik0)
vokrugplayertorpednik1 = building_area_around_player_ships(playertorpednik1)
vokrugplayertorpednik2 = building_area_around_player_ships(playertorpednik2)
vokrugplayertorpednik3 = building_area_around_player_ships(playertorpednik3)

playerlinkor = playerlinkor.split(',')
playerkreiser0 = playerkreiser0.split(',')
playerkreiser1 = playerkreiser1.split(',')
playeraesminec0 = playeraesminec0.split(',')
playeraesminec1 = playeraesminec1.split(',')
playeraesminec2 = playeraesminec2.split(',')
playertorpednik0 = [playertorpednik0]
playertorpednik1 = [playertorpednik1]
playertorpednik2 = [playertorpednik2]
playertorpednik3 = [playertorpednik3]

def move_realisation(opponent_battlefield, move):
	a = ''
	if opponent_battlefield[move] == ' #':
		a = ' +'
	elif opponent_battlefield[move] == ' #\n':
		a = ' +\n'
	elif opponent_battlefield[move] == ' ~':
		a = ' -'
	elif opponent_battlefield[move] == ' ~\n':
		a = ' -\n'
	else:
		a = opponent_battlefield[move]
	return a





def destroyed(move, opponent_battlefield, hidden_battlefield):
	lllist = []
	if move in linkor:			
		for i in linkor:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(linkor) == len(lllist):
			for i in vokruglinkora:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'
	if move in kreiser0:
		for i in kreiser0:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(kreiser0) == len(lllist):
			for i in vokrugkreiser0:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'	
	if move in kreiser1:
		for i in kreiser1:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(kreiser1) == len(lllist):
			for i in vokrugkreiser1:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'	
	if move in aesminec0:
		for i in aesminec0:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(aesminec0) == len(lllist):
			for i in vokrugaesminec0:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'		
	if move in aesminec1:
		for i in aesminec1:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(aesminec1) == len(lllist):
			for i in vokrugaesminec1:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'
	if move in aesminec2:
		for i in aesminec2:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(aesminec2) == len(lllist):
			for i in vokrugaesminec2:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'
	if move in torpednik0:
		for i in torpednik0:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(torpednik0) == len(lllist):
			for i in vokrugtorpednik0:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'
	if move in torpednik1:
		for i in torpednik1:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(torpednik1) == len(lllist):
			for i in vokrugtorpednik1:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'
	if move in torpednik2:
		for i in torpednik2:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(torpednik2) == len(lllist):
			for i in vokrugtorpednik2:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'
	if move in torpednik3:
		for i in torpednik3:
			if opponent_battlefield[i] == ' +' or  opponent_battlefield[i] == ' +\n':
				lllist.append(opponent_battlefield[i])
		if len(torpednik3) == len(lllist):
			for i in vokrugtorpednik3:
				if '\n' in hidden_battlefield[i]:
					hidden_battlefield[i] = ' -\n'
				else:
					hidden_battlefield[i] = ' -'

def destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, movelist):
	lllist = []
	if playerlinkor[0] in movelist:			
		for i in playerlinkor:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playerlinkor) == len(lllist):
			for i in vokrugplayerlinkor:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'
	if playerkreiser0[0] in movelist:
		for i in playerkreiser0:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playerkreiser0) == len(lllist):
			for i in vokrugplayerkreiser0:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'	
	if playerkreiser1[0] in movelist:
		for i in playerkreiser1:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playerkreiser1) == len(lllist):
			for i in vokrugplayerkreiser1:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'	
	if playeraesminec0[0] in movelist:
		for i in playeraesminec0:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playeraesminec0) == len(lllist):
			for i in vokrugaesminec0:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'		
	if playeraesminec1[0] in movelist:
		for i in playeraesminec1:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playeraesminec1) == len(lllist):
			for i in vokrugplayeraesminec1:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'
	if playeraesminec2[0] in movelist:
		for i in playeraesminec2:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playeraesminec2) == len(lllist):
			for i in vokrugplayeraesminec2:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'
	if playertorpednik0[0] in movelist:
		for i in playertorpednik0:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playertorpednik0) == len(lllist):
			for i in vokrugplayertorpednik0:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'
	if playertorpednik1[0] in movelist:
		for i in playertorpednik1:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playertorpednik1) == len(lllist):
			for i in vokrugplayertorpednik1:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'
	if playertorpednik2[0] in movelist:
		for i in playertorpednik2:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playertorpednik2) == len(lllist):
			for i in vokrugplayertorpednik2:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'
	if playertorpednik3[0] in movelist:
		for i in playertorpednik3:
			if player_battlefield_for_opponent_movelist[i] == ' +' or  player_battlefield_for_opponent_movelist[i] == ' +\n':
				lllist.append(player_battlefield_for_opponent_movelist[i])
		if len(playertorpednik3) == len(lllist):
			for i in vokrugplayertorpednik3:
				if '\n' in player_battlefield_for_opponent_movelist[i]:
					player_battlefield_for_opponent_movelist[i] = ' -\n'
				else:
					player_battlefield_for_opponent_movelist[i] = ' -'

finalmovelist = []
player_battlefield_for_opponent_movelist = player_battlefield.copy()
while ' #' in player_battlefield_for_opponent_movelist.values() or ' #\n' in player_battlefield_for_opponent_movelist.values():
	integer = randint(1, 10)
	letter = randint(97, 106)
	random_move = str(integer) + chr(letter)
	while player_battlefield_for_opponent_movelist[random_move] == ' -' or	player_battlefield_for_opponent_movelist[random_move] == ' -\n':
		integer = randint(1, 10)
		letter = randint(97, 106)
		random_move = str(integer) + chr(letter)
	switch_direction = 0
	movelist = []
	movelist1 = []
	movelist2 = []
	movelist3 = []
	movelist.append(random_move)

	if player_battlefield_for_opponent_movelist[random_move] == ' #':

		if player_battlefield_for_opponent_movelist[movelist[-1]] == ' #':
			n = -1
			b = movelist[-1].replace(random_move[-1], chr(ord(random_move[-1]) + n))
			movelist.append(b)
			destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
			if '`' not in b and player_battlefield_for_opponent_movelist[b] == ' #':
				b = movelist[-1].replace(movelist[-1][-1], chr(ord(movelist[-1][-1]) + n))
				movelist.append(b)	
				destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
				if '`' not in b and player_battlefield_for_opponent_movelist[b] == ' #':	
					b = movelist[-1].replace(movelist[-1][-1], chr(ord(movelist[-1][-1]) + n))
					movelist.append(b)			
					destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
					if '`' not in b and player_battlefield_for_opponent_movelist[b] == ' #':
						b = movelist[-1].replace(movelist[-1][-1], chr(ord(movelist[-1][-1]) + n))
						movelist.append(b)
						destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
					else:
						switch_direction = 1
				else:
					switch_direction = 1	
			else:
				switch_direction = 1	

		movelist1 = movelist
		if switch_direction == 1:
			movelist = []
			movelist.append(random_move)
			b = movelist[-1].replace(random_move[-1], chr(ord(random_move[-1]) + 1))
			movelist.append(b)
			destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
			if player_battlefield_for_opponent_movelist[b] == ' #':
				b = movelist[-1].replace(movelist[-1][-1], chr(ord(movelist[-1][-1]) + 1))
				movelist.append(b)	
				destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
				if player_battlefield_for_opponent_movelist[b] == ' #':	
					b = movelist[-1].replace(movelist[-1][-1], chr(ord(movelist[-1][-1]) + 1))
					movelist.append(b)			
					destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
					if player_battlefield_for_opponent_movelist[b] == ' #':	
						b = movelist[-1].replace(movelist[-1][-1], chr(ord(movelist[-1][-1]) + 1))
						movelist.append(b)
						destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
					else:
						switch_direction = 3
				else:
					switch_direction = 3
			else:
				switch_direction = 3	
		movelist2 = movelist
		if switch_direction == 3:
			movelist = []
			movelist.append(random_move)
			b = movelist[-1].replace(random_move[:-1], str(int(random_move[:-1]) - 1))
			movelist.append(b)
			destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
			if b[0] != '0' and player_battlefield_for_opponent_movelist[b] == ' #':
				b = movelist[-1].replace(movelist[-1][:-1], str(int(movelist[-1][:-1]) - 1))
				movelist.append(b)	
				destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
				if b[0] != '0' and player_battlefield_for_opponent_movelist[b] == ' #':
					b = movelist[-1].replace(movelist[-1][:-1], str(int(movelist[-1][:-1]) - 1))
					movelist.append(b)			
					destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
					if b[0] != '0' and player_battlefield_for_opponent_movelist[b] == ' #':
						b = movelist[-1].replace(movelist[-1][:-1], str(int(movelist[-1][:-1]) - 1))
						movelist.append(b)
						destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
					else:
						switch_direction = 4
				else:
					switch_direction = 4
			else:
				switch_direction = 4

		movelist3 = movelist
		if switch_direction == 4:
			movelist = []
			movelist.append(random_move)
			b = movelist[-1].replace(random_move[:-1], str(int(random_move[:-1]) + 1))
			movelist.append(b)
			destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
			if '11' not in b and player_battlefield_for_opponent_movelist[b] == ' #' :
				b = movelist[-1].replace(movelist[-1][:-1], str(int(movelist[-1][:-1]) + 1))
				movelist.append(b)	
				destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
				if '11' not in b and player_battlefield_for_opponent_movelist[b] == ' #':
					b = movelist[-1].replace(movelist[-1][:-1], str(int(movelist[-1][:-1]) + 1))
					movelist.append(b)			
					destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
					if '11' not in b and player_battlefield_for_opponent_movelist[b] == ' #':
						b = movelist[-1].replace(movelist[-1][:-1], str(int(movelist[-1][:-1]) + 1))
						movelist.append(b)
						destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, b)
	movelist = movelist2 + movelist1 + movelist3 + movelist

	seen = set()
	seen_add = seen.add
	movelist = [x for x in movelist if not (x in seen or seen_add(x))]



	while '`' in ''.join(movelist) or 'k' in ''.join(movelist) or '11' in ''.join(movelist):
		for i in range(len(movelist)):
			if 'k' in movelist[i]:
				movelist.pop(i)
				break
			elif '`' in movelist[i]:
				movelist.pop(i)
				break
			elif '11' in movelist[i]:
				movelist.pop(i)
				break


	if '10' not in ''.join(movelist):
		while '0' in ''.join(movelist):
			for i in range(len(movelist)):
				if '0' in movelist[i]:
					movelist.pop(i)
					break

	countpluses = 0
	for i in movelist:
		if player_battlefield_for_opponent_movelist[i] == ' #' or player_battlefield_for_opponent_movelist[i] == ' #\n':
			countpluses += 1

	if countpluses == 0:
		if player_battlefield_for_opponent_movelist[movelist[0]] == ' ~':
			finalmovelist.append(movelist[0]) 
		elif player_battlefield_for_opponent_movelist[movelist[0]] == ' ~\n':
			finalmovelist.append(movelist[0]) 

	for i in movelist:
		if player_battlefield_for_opponent_movelist[i] == ' #':
			player_battlefield_for_opponent_movelist[i] = ' +' 
			finalmovelist.append(i) 
		elif player_battlefield_for_opponent_movelist[i] == ' #\n':
			player_battlefield_for_opponent_movelist[i] = ' +\n' 
			finalmovelist.append(i) 

	if len(movelist) > 1:
		destroyed_for_player_battlefield_for_opponent_movelist(player_battlefield_for_opponent_movelist, movelist)		

	for i in finalmovelist:
		if player_battlefield_for_opponent_movelist[i] == ' #':
			player_battlefield_for_opponent_movelist[i] = ' +' 
		elif player_battlefield_for_opponent_movelist[i] == ' #\n':
			player_battlefield_for_opponent_movelist[i] = ' +\n' 
		elif player_battlefield_for_opponent_movelist[i] == ' ~':
			player_battlefield_for_opponent_movelist[i] = ' -'
		elif player_battlefield_for_opponent_movelist[i] == ' ~\n':
			player_battlefield_for_opponent_movelist[i] = ' -\n' 

def kak_ge_uge_zeblo(player_battlefield, opponent_move):
	lllist = []
	if opponent_move in playerlinkor:			
		for i in playerlinkor:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playerlinkor) == len(lllist):
			for i in vokrugplayerlinkor:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'
	if opponent_move in playerkreiser0:
		for i in playerkreiser0:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playerkreiser0) == len(lllist):
			for i in vokrugplayerkreiser0:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'	
	if opponent_move in playerkreiser1:
		for i in playerkreiser1:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playerkreiser1) == len(lllist):
			for i in vokrugplayerkreiser1:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'	
	if opponent_move in playeraesminec0:
		for i in playeraesminec0:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playeraesminec0) == len(lllist):
			for i in vokrugaesminec0:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'		
	if opponent_move in playeraesminec1:
		for i in playeraesminec1:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playeraesminec1) == len(lllist):
			for i in vokrugplayeraesminec1:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'
	if opponent_move in playeraesminec2:
		for i in playeraesminec2:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playeraesminec2) == len(lllist):
			for i in vokrugplayeraesminec2:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'
	if opponent_move in playertorpednik0:
		for i in playertorpednik0:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playertorpednik0) == len(lllist):
			for i in vokrugplayertorpednik0:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'
	if opponent_move in playertorpednik1:
		for i in playertorpednik1:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playertorpednik1) == len(lllist):
			for i in vokrugplayertorpednik1:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'
	if opponent_move in playertorpednik2:
		for i in playertorpednik2:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playertorpednik2) == len(lllist):
			for i in vokrugplayertorpednik2:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'
	if opponent_move in playertorpednik3:
		for i in playertorpednik3:
			if player_battlefield[i] == ' +' or  player_battlefield[i] == ' +\n':
				lllist.append(player_battlefield[i])
		if len(playertorpednik3) == len(lllist):
			for i in vokrugplayertorpednik3:
				if '\n' in player_battlefield[i]:
					player_battlefield[i] = ' -\n'
				else:
					player_battlefield[i] = ' -'

os.system('cls')
os.system('clear')
print('\n       Твое поле боя\n')
print('    a b c d e f g h i j\n')
print(''.join(player_battlefield.values()))
print('-----------------------------')
print('    Поле боя противника\n')
print('    a b c d e f g h i j\n')
print(''.join(hidden_battlefield.values()))

n = 0
while True:
	if n >= len(finalmovelist):
		n = 0
	move = input()
	opponent_move = finalmovelist[n]	
	if move in opponent_battlefield.keys():		
		opponent_battlefield[move] = move_realisation(opponent_battlefield, move)
		hidden_battlefield[move] = opponent_battlefield[move]
		destroyed(move, opponent_battlefield, hidden_battlefield)
		player_battlefield[opponent_move] = move_realisation(player_battlefield, opponent_move)	
		kak_ge_uge_zeblo(player_battlefield, opponent_move)		
	elif move == 'stop':
		print('\n    See u soon')
		break		
	else:
		try:
			if move[0].isdigit() == False:
				move = move[::-1]		
				opponent_battlefield[move] = move_realisation(opponent_battlefield, move)
				hidden_battlefield[move] = opponent_battlefield[move]
				destroyed(move, opponent_battlefield, hidden_battlefield)
				player_battlefield[opponent_move] = move_realisation(player_battlefield, opponent_move)
				kak_ge_uge_zeblo(player_battlefield, opponent_move)	
			elif move == 'stop':
				print('\n    See u soon')
				break
		except KeyError:
			player_battlefield[opponent_move] = move_realisation(player_battlefield, opponent_move)
			kak_ge_uge_zeblo(player_battlefield, opponent_move)	
			print('\nВвод должен быть в формате:\nцифра + буква или буква + цифра\n')
		except IndexError:
			player_battlefield[opponent_move] = move_realisation(player_battlefield, opponent_move)
			kak_ge_uge_zeblo(player_battlefield, opponent_move)	
			print('\nВвод должен быть в формате:\nцифра + буква или буква + цифра\n') 
	os.system('cls')
	os.system('clear')
	print('\n       Твое поле боя\n')
	print('    a b c d e f g h i j\n')
	print(''.join(player_battlefield.values()))
	print('-----------------------------')
	print('    Поле боя противника\n')
	print('    a b c d e f g h i j\n')
	print(''.join(hidden_battlefield.values()))
	n += 1
	
	if ' #' not in opponent_battlefield.values() and ' #\n' not in opponent_battlefield.values():
		print('        Победка!')
		break
	elif ' #' not in player_battlefield.values() and ' #\n' not in player_battlefield.values():
		print('        Поражение!')
		break